import json
import time
import webbrowser
from http import HTTPStatus

from requests_html import HTMLSession, HTMLResponse


def get_product_id(session: HTMLSession, url: str) -> int:
    page = session.get(url)
    page.raise_for_status()

    for function in [get_id_from_product_select, get_id_from_product_json]:
        try:
            return function(page)
        except Exception:
            pass
    
    raise ValueError("Could not find unique id with any of the existing methods")


def get_id_from_product_select(page: HTMLResponse) -> int:
    option_elts = page.html.find(
        "select#ProductSelect-product-template > option"
    )
    if not option_elts:
        raise ValueError("Could not find id from ProductSelect")
    elif len(option_elts) > 1:
        raise ValueError("Multiple variants found")

    return int(option_elts[0].attrs['value'])


def get_id_from_product_json(page: HTMLResponse) -> int:
    script_elt = page.html.find(
        "script#ProductJson-product-template",
        first=True,
    )
    if script_elt is None:
        raise ValueError("Could not find id from ProductJson")
    
    content = json.loads(script_elt.text)
    variants = content['variants']
    if len(variants) > 1:
        raise ValueError("Multiple variants found")

    return variants[0]['id']


ADD_TO_CART_URL = "https://shop.steprevolution.com/cart/add.js"

def add_to_cart(session: HTMLSession, product_id: int, quantity: int) -> None:
    r = session.post(ADD_TO_CART_URL, data={'id': product_id, 'quantity': quantity})
    r.raise_for_status()


def create_cart(product_id: int, quantity: int) -> str:
    """Return cart id cookie"""
    session = HTMLSession()
    add_to_cart(session, product_id, quantity)
    return session.cookies['cart']


CART_URL = "https://shop.steprevolution.com/cart"

def checkout(session: HTMLSession) -> str:
    """Return the checkout url"""
    r = session.post(CART_URL, data={"checkout": ""}, allow_redirects=False)
    r.raise_for_status()
    assert r.status_code == HTTPStatus.FOUND
    return r.headers['Location']


def checkout_is_possible(session: HTMLSession, checkout_url: str) -> bool:
    r = session.head(checkout_url, allow_redirects=False)
    return r.status_code == HTTPStatus.OK


def order(url: str, quantity: int) -> None:
    """Places an order for the specified `quantity` of the item on page `url`"""
    session = HTMLSession()
    print("Getting product id")
    product_id = get_product_id(session, url)
    print(f"Product id : {product_id}")
    print(f"Creating cart")
    add_to_cart(session, product_id, quantity)
    checkout_url = checkout(session)
    while not checkout_is_possible(session, checkout_url):
        print("Checkout does not look possible ...")
        time.sleep(1)
    print("CHECKOUT LOOKS POSSIBLE, OPENING BROWSER")
    webbrowser.open_new_tab(checkout_url)


if __name__ == "__main__":
    order(
        "https://shop.steprevolution.com/products/stepmaniax-stage-5th-generation",
        2
    )